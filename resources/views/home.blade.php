<!DOCTYPE html>
<html lang="{{ app()->getLocale() }}">

<head>
	<meta charset="utf-8">
	<meta http-equiv="X-UA-Compatible" content="IE=edge">
	<meta name="viewport" content="width=device-width, initial-scale=1">
	<meta name="csrf-token" content="{{ csrf_token() }}">

	<link rel="stylesheet" type="text/css" href="{{ asset('css/app.css') }}">

	<title>eMyPeople API Server</title>

</head>


<body
	class="pt-3"
>{{-- padding-top to clear fixed-top MenuBar --}}

	<div id="app"></div>

	<script src="{{ asset('js/app.js') }}"></script>
</body>

</html>