import Vue from 'vue';
import Vuex from 'vuex';
	Vue.use(Vuex);


export default new Vuex.Store({
	state: {
		menubar_items: {
			auth: {
				title: 'Authorization',
				id: 'navbarDropdownMenuLink0',
				submenu_items: {
					register: {
						title: 'Authentication',
						linkTo: '/auth/register',
						pills: [
							{
								title: 'Authentication',
								class: 'nav-link active',
								id: 'v-pills-authentication-tab',
								path: '#v-pills-authentication',
								linkTo: '/auth/register',
							},
							{
								title: 'Authorization',
								class: 'nav-link',
								id: 'v-pills-authorization-tab',
								path: '#v-pills-authorization',
								linkTo: '/auth/access',
							},
						],
						accords: [
							{
								blurb: '<p>To use the API server, you must first register an API <em>consumer</em>, much the same as a logon in a traditional server context.</p>',
								class: 'tab-pane fade show active',
								id: 'v-pills-authentication',
								idocClass: 'collapse',
								exuiBtnClass: 'btn btn-link',
								exuiClass: 'collapse show',  // NB: unique.
								tabs: [
									{
										verb: 'post',
										tip: 'create',
										classes: 'nav-link active',
										aria_select: true,
									},
									{
										verb: 'get',
										tip: 'read',
										classes: 'nav-link disabled',
										aria_select: false,
									},
									{
										verb: 'put',
										tip: 'update',
										classes: 'nav-link disabled',
										aria_select: false,
									},
									{
										verb: 'delete',
										tip: 'destroy',
										classes: 'nav-link disabled',
										aria_select: false,
									},
								],
							},
/*							{
								class: 'tab-pane fade',
								id: 'v-pills-authorization',
								idocClass: 'collapse show',
								exuiClass: 'collapse',
							},
*/						],
					},
					access: {
						title: 'Authorization',
						linkTo: '/auth/access',
						pills: [
							{
								title: 'Authentication',
								class: 'nav-link',
								id: 'v-pills-authentication-tab',
								path: '#v-pills-authentication',
								linkTo: '/auth/register',
							},
							{
								title: 'Authorization',
								class: 'nav-link active',
								id: 'v-pills-authorization-tab',
								path: '#v-pills-authorization',
								linkTo: '/auth/access',
							},
						],
						accords: [
/*							{
								class: 'tab-pane fade',
								id: 'v-pills-authentication',
								idocClass: 'collapse',
								exuiClass: 'collapse show',  // NB: unique.
							},
*/							{
								blurb: '<p>Each endpoint you can reach on the server, and each action you can perform on each endpoint, is guarded in such a way that only API calls bearing a 60-character token <em>scoped</em> to the relevant privileges will be executed. Thus a call requesting a list of router security policies would have to bear a token scoped to the Read Policies [<code>r-pols</code>] privilege: <em>i.e.,</em> access to a <code>[...]/policies/</code> endpoint with permission to <code>GET</code> from the endpoint.</p></p>',
								class: 'tab-pane fade show active',
								id: 'v-pills-authorization',
								idocClass: 'collapse show',
								exuiBtnClass: 'btn btn-link',
								exuiClass: 'collapse',
								tabs: [
									{
										verb: 'post',
										tip: 'create',
										classes: 'nav-link active',
										aria_select: true,
									},
									{
										verb: 'get',
										tip: 'read',
										classes: 'nav-link disabled',
										aria_select: false,
									},
									{
										verb: 'put',
										tip: 'update',
										classes: 'nav-link disabled',
										aria_select: false,
									},
									{
										verb: 'delete',
										tip: 'destroy',
										classes: 'nav-link disabled',
										aria_select: false,
									},
								],
							},
						],
					},
				}
			},
			gateway: {
				title: 'Juniper SRX 100 Gateway',
				id: 'navbarDropdownMenuLink1',
				submenu_items: {
					policies: {
						title: 'Security Policies',
						linkTo: '/gateway/policies',
						pills: [
							{
								title: 'Security Policies',
								class: 'nav-link active',
								id: 'v-pills-policies-tab',
								path: '#v-pills-policies',
								linkTo: '/gateway/policies',
							},
							{
								title: 'Address-Set Pools',
								class: 'nav-link',
								id: 'v-pills-pools-tab',
								path: '#v-pills-pools',
								linkTo: '/gateway/pools',
							},
							{
								title: 'Transparent Proxies',
								class: 'nav-link',
								id: 'v-pills-proxies-tab',
								path: '#v-pills-proxies',
								linkTo: '/gateway/proxies',
							},
						],
						accords: [
							{
								blurb: '<p>Something something something something.</p>',
								class: 'tab-pane fade show active',
								id: 'v-pills-policies',
								idocClass: 'collapse show',
								exuiBtnClass: 'btn btn-link',
								exuiClass: 'collapse',
								tabs: [
									{
										verb: 'post',
										tip: 'create',
										classes: 'nav-link disabled',
										aria_select: false,
									},
									{
										verb: 'get',
										tip: 'read',
										classes: 'nav-link active',
										aria_select: true,
									},
									{
										verb: 'put',
										tip: 'update',
										classes: 'nav-link',
										aria_select: false,
									},
									{
										verb: 'delete',
										tip: 'destroy',
										classes: 'nav-link disabled',
										aria_select: false,
									},
								],
							},
/*							{
								class: 'tab-pane fade',
								id: 'v-pills-pools',
								idocClass: 'collapse show',
								exuiClass: 'collapse',
							},
							{
								class: 'tab-pane fade',
								id: 'v-pills-proxies',
								idocClass: 'collapse show',
								exuiClass: 'collapse',
							},
*/						],
					},
					pools: {
						title: 'Address-Set Pools',
						linkTo: '/gateway/pools',
						pills: [
							{
								title: 'Security Policies',
								class: 'nav-link',
								id: 'v-pills-policies-tab',
								path: '#v-pills-policies',
								linkTo: '/gateway/policies',
							},
							{
								title: 'Address-Set Pools',
								class: 'nav-link active',
								id: 'v-pills-pools-tab',
								path: '#v-pills-pools',
								linkTo: '/gateway/pools',
							},
							{
								title: 'Transparent Proxies',
								class: 'nav-link',
								id: 'v-pills-proxies-tab',
								path: '#v-pills-proxies',
								linkTo: '/gateway/proxies',
							},
						],
						accords: [
/*							{
								class: 'tab-pane fade',
								id: 'v-pills-policies',
								idocClass: 'collapse show',
								exuiClass: 'collapse',
							},
*/							{
								blurb: '<p>Something something something something.</p>',
								class: 'tab-pane fade show active',
								id: 'v-pills-pools',
								idocClass: 'collapse show',
								exuiBtnClass: 'btn btn-link disabled',
								exuiClass: 'collapse',
								tabs: [
									{
										verb: 'post',
										tip: 'create',
										classes: 'nav-link disabled',
										aria_select: false,
									},
									{
										verb: 'get',
										tip: 'read',
										classes: 'nav-link active',
										aria_select: true,
									},
									{
										verb: 'put',
										tip: 'update',
										classes: 'nav-link',
										aria_select: false,
									},
									{
										verb: 'delete',
										tip: 'destroy',
										classes: 'nav-link disabled',
										aria_select: false,
									},
								],
							},
/*							{
								class: 'tab-pane fade',
								id: 'v-pills-proxies',
								idocClass: 'collapse show',
								exuiClass: 'collapse',
							},
*/						],
					},
					proxies: {
						title: 'Transparent Proxies',
						linkTo: '/gateway/proxies',
						pills: [
							{
								title: 'Security Policies',
								class: 'nav-link',
								id: 'v-pills-policies-tab',
								path: '#v-pills-policies',
								linkTo: '/gateway/policies',
							},
							{
								title: 'Address-Set Pools',
								class: 'nav-link',
								id: 'v-pills-pools-tab',
								path: '#v-pills-pools',
								linkTo: '/gateway/pools',
							},
							{
								title: 'Transparent Proxies',
								class: 'nav-link active',
								id: 'v-pills-proxies-tab',
								linkTo: '/gateway/proxies',
								path: '#v-pills-proxies',
							},
						],
						accords: [
/*							{
								class: 'tab-pane fade',
								id: 'v-pills-policies',
								idocClass: 'collapse show',
								exuiClass: 'collapse',
							},
							{
								class: 'tab-pane fade',
								id: 'v-pills-pools',
								idocClass: 'collapse show',
								exuiClass: 'collapse',
							},
*/							{
								blurb: '<p>Something something something something.</p>',
								class: 'tab-pane fade show active',
								id: 'v-pills-proxies',
								idocClass: 'collapse show',
								exuiBtnClass: 'btn btn-link',
								exuiClass: 'collapse',
								tabs: [
									{
										verb: 'post',
										tip: 'create',
										classes: 'nav-link disabled',
										aria_select: false,
									},
									{
										verb: 'get',
										tip: 'read',
										classes: 'nav-link active',
										aria_select: true,
									},
									{
										verb: 'put',
										tip: 'update',
										classes: 'nav-link',
										aria_select: false,
									},
									{
										verb: 'delete',
										tip: 'destroy',
										classes: 'nav-link disabled',
										aria_select: false,
									},
								],
							},
						],
					},
				}
			},
			mail: {
				title: 'Zimbra Email Server',
				id: 'navbarDropdownMenuLink2',
				submenu_items: {
					soon: {
						title: 'Coming soon!',
						linkTo: '#',
					},
				},
			},
		},
	},
});