import Vue from 'vue';
import VueRouter from 'vue-router';
	Vue.use(VueRouter);
import { fake404 } from './helpers';
import DocRegistration from './components/DocRegistration.vue';
import UiRegistration from './components/UiRegistration.vue';
import DocAccess from './components/DocAccess.vue';
import DocPolicy from './components/DocPolicy.vue';
import UiPolicy from './components/UiPolicy.vue';
import DocPool from './components/DocPool.vue';
import DocProxy from './components/DocProxy.vue';
import UiProxy from './components/UiProxy.vue';


let router = new VueRouter({
	mode: 'history',
	routes: [
		{
			path: '/auth/register',
			components: {
				'doc': DocRegistration,
				'ui': UiRegistration,
			},
		},
		{
			path: '/auth/access',
			components: {
				'doc': DocAccess,
				'ui': UiRegistration,
			},
		},
		{
			path: '/gateway/policies',
			components: {
				'doc': DocPolicy,
				'ui': UiPolicy,
			},
		},
		{
			path: '/gateway/pools',
			components: {
				'doc': DocPool,
				// 'ui': UiPool,
			},
		},
		{
			path: '/gateway/proxies',
			components: {
				'doc': DocProxy,
				'ui': UiProxy,
			},
		},
		// { path: '/mail/...', },
		{ path: '/', redirect: '/auth/register' },
		{ path: '*', beforeEnter: (to, from, next) => {
			fake404();
			next(false);
		}},
	],
	scrollBehavior (to, from, savedPosition) {
		if (savedPosition) {
			return savedPosition;
		} else {
			return { x: 0, y: 0 };
		}
	},
});

export default router;